//Add solution here

const http = require('http');

const app = 3000;

const server = http.createServer((request, response) => {

	if(request.url == '/login') {
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end(`Welcome to the login page.`);
	} if(request.url == '/register') {
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end(`I'm sorry the page you are looking for cannot be found.`);
	} else {
		response.writeHead(404, {'Content-Type': 'text/plain'});
		response.end(`Error 404: Page not found!`  );
	};
});
server.listen(app, () => {
  console.log(`Server is now accessible at localhost:${app}.`);
});

//Do not modify
module.exports = {app}